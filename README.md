# Deterministic Finite Automaton Simulation

For this proejct, I wrote a program that simulates the computation of a DFA on a series of input strings, and reports the results of those computations 
The program input is a file that (1) describes the DFA to simulate in terms of the number of states, alphabet, transition function, start state, and accept states, and (2) specifies a series of strings for which the DFA’s computation should be simulated. 
The program output is the result of each computation as “Accept” if the DFA accepts the input string, and “Reject” if the DFA rejects the input string. 