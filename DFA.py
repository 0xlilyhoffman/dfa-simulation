"""
pa1.py
Automata Spring 2017
Programming Assignment 1 - DFA
March 3, 2017

Lily Hoffman

Simulates the computation of a DFA on various input strings
DFA configurations (states, alphabet, transition function, start state,  accepting states) 
	as well as input strings are read in from a file
Program displays to user whether the input string was accepted or rejected by the DFA

"""


import sys
import re

def parseInfile(file):
	############################################################################
	#                                                                          #
    #  Unpacks input file and returns a 5-tuple DFA and list of input strings  #
	#                                                                          #
	############################################################################

    f = open(file)

    #States - int
    num_states = int(f.readline())

    #Symbols - character string
    alphabet = f.readline()
    alphabet = alphabet.strip()

    #Transition function = list of 'q1 a q2' elements
    transition_function = list()

    reading_transition_function = True
    while(reading_transition_function):
        line = f.readline()
        if len(line) < 3:
            reading_transition_function = False
        else:
            transition_function.append(line)

    #Start state = int
    start_state = int(line[0])

    #Accept state = list of ints
    accept_states_line = f.readline()
    accept_states = list()
    accept_states_line = str(accept_states_line).split()
    for state in accept_states_line:
        if state != ' ' and state != '\n':
            accept_states.append(int(state))

    #Input strings - list of strings 
    input_strings = list()
    reading_file = True
    while(reading_file):
        line = f.readline()
        if(line == ''):
           reading_file = False
        else:
            input_strings.append(line)

    return(num_states, alphabet, transition_function,start_state,accept_states, input_strings)

def runDFA():
	############################################################################
	#                                                                          #
	# Creates a DFA from parameters from parseInfile                           #
	# 	and runs input strings through DFA by transitioning to the next state  #
	#   based on current state and current symbol                              #
	# Prints "accept" or "reject" based on final state of DFA for the given    #
	# 	input string and given set of accept states							   #
	#                                                                          #
	############################################################################


    num_states, alphabet, transition_function, start_state, accept_states, input_strings = parseInfile(sys.argv[1])

    #Create transition table mapping (current_state, current_symbol) -> next_state
    transition_table = {}
    for transition in transition_function:
        regex_match = re.match("(\d+) '(.)' (\d+)", transition)
        in_state = int(regex_match.group(1))
        reading_symbol = regex_match.group(2)
        to_state = int(regex_match.group(3))
        transition_table[(in_state, reading_symbol)] = to_state


    #Read input string
    for input_string in input_strings:
        current_state = start_state
        i = 0
        while i < len(input_string):
            current_symbol = input_string[i]
            if current_symbol == '\n':
                break
            key = (current_state, current_symbol)
            next_state = transition_table.get(key)
            current_state = next_state
            i+=1


        accept = False
        for state in accept_states:
            if current_state == state:
                accept = True
                break
		#Print result to user
        if(accept):
            print("Accept")
        else:
            print("Reject")



if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python pa1.py [infile]")
    else:
        runDFA()

